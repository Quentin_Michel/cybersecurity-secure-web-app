import Route from './route';
import Admin from './pages/admin';
import Home from './pages/index';
import Contact from './pages/contact';
import Pricing from './pages/pricing';
import Geo from './pages/geo';
import Feedback from './pages/feedback';
import Login from './pages/login';

import './css/reset.scss';
import './css/bootstrap.scss';
import './index.scss';

// eslint-disable-next-line no-unused-vars
const routeHome = new Route('/', Home);
// eslint-disable-next-line no-unused-vars
const routeContact = new Route('/contact', Contact);
// eslint-disable-next-line no-unused-vars
const routePricing = new Route('/pricing', Pricing);
// eslint-disable-next-line no-unused-vars
const routeGeo = new Route('/geo', Geo);
// eslint-disable-next-line no-unused-vars
const routeFeedback = new Route('/feedback', Feedback);
// eslint-disable-next-line no-unused-vars
const routeAdmin = new Route('/admin', Admin, true);
// eslint-disable-next-line no-unused-vars
const routeLogin = new Route('/login', Login);

// console.log(routeContact, routeHome, routePricing, routeGeo, routeFeedback, routeAdmin, routeLogin);
