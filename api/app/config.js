require('dotenv').config();

module.exports = {
  development: {
    type: 'development',
    port: 3000,
    mongodb: process.env.DATABASE_URL 
  },
  production: {
    type: 'production',
    port: 3000,
    mongodb: process.env.DATABASE_URL
  }
}