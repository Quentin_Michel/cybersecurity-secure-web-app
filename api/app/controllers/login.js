const UserModel = require('../models/user.js');
const jwt = require('jsonwebtoken');

const Login = class Login {
  /**
   * @constructor
   * @param {Object} app
   * @param {Object} connect
   */
  constructor(app, connect) {
    this.app = app;
    this.UserModel = connect.model('User', UserModel);

    this.run();
  }

  
  auth() {
    this.app.get('/auth/', (req, res) => {
      try {
        res.status(200).json({ 'message': 'ok' })
      } catch (err) {
        console.error(`[ERROR] POST logins/ -> ${err}`)
  
        res.status(500).json({
          code: 500,
          message: 'Internal server error'
        })
      }
    })
  }


  getByLoginPassword() {
    this.app.post('/login', async (req, res) => {
      try {
        const { name, password } = req.body;

        const user = await this.UserModel.findOne({ name: name });
        if (!user) {
          return res.status(401).json({ message: 'Utilisateur non trouvé' });
        }

        if (user.password !== password) {
          return res.status(401).json({ message: 'Mot de passe incorrect' });
        }

        const token = jwt.sign({ id: user._id, status: user.status}, 'test', { expiresIn: '1h' });

        res.status(200).json({ code: 200, message: 'Connexion réussie', token: token });
      } catch (erreur) {
        console.error(`[ERROR] POST /login/ -> ${erreur}`);
        res.status(500).json({
          code: 500,
          message: 'Erreur interne du serveur',
        });
      }
    });
  }

  /**
   * Run
   */
  run() {
    this.getByLoginPassword();
  }
};

module.exports = Login;
